﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace NPP
{
    public class DataLoader
    {
        private CancellationToken _cancellationToken;
        private Action<Row> _add;

        public DataLoader(Action<Row> add)
        {
            _add = add;
        }

        public event Action Loaded;

        public async void Load(string fileName, CancellationToken token)
        {
            _cancellationToken = token;

            using (var parser = new Parser(fileName))
            {
                await Task.Run(() => DoProcessing(parser));
            }

            Loaded();
        }

        public void DoProcessing(Parser parser)
        {
            while (!parser.EndOfFile)
            {
                if (_cancellationToken.IsCancellationRequested)
                    break;

                var row = parser.GetRow();
                if (row != null)
                    _add(row);
            }
        }
    }
}
