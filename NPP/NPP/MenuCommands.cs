﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using NPP.Properties;
using System.Runtime.CompilerServices;
using Prism.Commands;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using JetBrains.Annotations;
using System.Threading;
using System.IO;

namespace NPP
{
    public class MenuCommands : INotifyPropertyChanged
    {
        private const long MaxFileSize = 540000000;

        private readonly DelegateCommand _openCommand;
        private readonly DelegateCommand _closeCommand;
        private readonly DelegateCommand _exitCommand;

        private bool _isLoading;

        private string _currentFileName;
        private readonly DataLoader _dataLoader;
        private readonly Dialogs _dialogs;
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MenuCommands(Dialogs dialogs, Action close, Action<Row> add)
        {
            _dialogs = dialogs;
            _dataLoader = new DataLoader(add);
            _dataLoader.Loaded += OnLoaded;

            _openCommand = new DelegateCommand(Open);
            _closeCommand = new DelegateCommand(Close, CanClose);
            _exitCommand = new DelegateCommand(close);

            IsLoading = false;
        }

        public ICommand OpenCommand => _openCommand;

        public ICommand CloseCommand => _closeCommand;

        public ICommand ExitCommand => _exitCommand;

        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                OnPropertyChanged();
            }
        }

        public void RaiseCanExecute()
        {
            _closeCommand.RaiseCanExecuteChanged();
        }

        public void OnLoaded()
        {
            IsLoading = false;

            _dialogs.CloseProgress();
        }

        private void Open()
        {
            if (!string.IsNullOrEmpty(_currentFileName))
            {
                Close();
            }

            var openDialog = new OpenFileDialog
            {
                DefaultExt = Resources.DefaultExt
            };

            if (openDialog.ShowDialog() == true)
            {
                _currentFileName = openDialog.FileName;
                var fileStream = new FileStream(_currentFileName, FileMode.Open);

                IsLoading = true;

                var cancellationToken = new CancellationTokenSource();

                //if (GetFileSize(fileStream) >= MaxFileSize)
                //{
                //    var dialogResult = _dialogs.ShowWarning("Вы выбрали файл очень большого размера. Вы уверены, что хотите продолжить?", "Warning");

                //    if (!dialogResult)
                //        return;
                //}

                _dataLoader.Load(_currentFileName, cancellationToken.Token);

                _dialogs.ShowProgress(cancellationToken);
            }
        }

        private static long GetFileSize(FileStream fileStream)
        {
            var fileSize = fileStream.Seek(0, SeekOrigin.End) - 1;

            fileStream.Seek(0, SeekOrigin.Begin);

            return fileSize;
        }

        private void Close()
        {
            _currentFileName = string.Empty;
        }

        private void CloseFile()
        {
            _currentFileName = string.Empty;

            //todo clean data
        }

        private bool CanClose()
        {
            return !string.IsNullOrEmpty(_currentFileName);
        }
    }
}