﻿using System;
using System.Threading;
using System.Windows;

namespace NPP
{
    public class Dialogs
    {
        private readonly Window _owner;
        private ProgressDialog _progressDialog;

        public Dialogs(Window owner)
        {
            _owner = owner;
        }

        public void ShowProgress(CancellationTokenSource cancellationToken)
        {
            _progressDialog = new ProgressDialog(cancellationToken, _owner);
            _progressDialog.ShowDialog();
        }

        public void CloseProgress()
        {
            _progressDialog?.Close();
        }

        public bool ShowWarning(string title, string message)
        {
            return MessageBox.Show(_owner, title, message, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes;
        }
    }
}
