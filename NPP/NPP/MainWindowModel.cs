﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace NPP
{
    public class MainWindowModel
    {
        public MainWindowModel(MainWindow mainWindow, Action close)
        {
            var dialogs = new Dialogs(mainWindow);
            Collection = new ObservableCollection<Row>();
            MenuCommands = new MenuCommands(dialogs, close, AddItems);
        }

        public MenuCommands MenuCommands { get; }

        public ObservableCollection<Row> Collection { get; set; }

        private void AddItems(Row item)
        {
            Application.Current.Dispatcher.Invoke(() => Collection.Add(item));
        }
    }
}
