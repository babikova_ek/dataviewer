﻿using JetBrains.Annotations;
using Prism.Commands;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Input;

namespace NPP
{
    public class ProgressDialogModel : INotifyPropertyChanged
    {
        private readonly CancellationTokenSource _cancelationToken;
        private readonly DelegateCommand _cancelCommand;
        private bool _isCancelling;

        public ProgressDialogModel(CancellationTokenSource cancelationToken)
        {
            _cancelationToken = cancelationToken;
            _cancelCommand = new DelegateCommand(Cancel);

            IsCancelling = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand CancelCommand => _cancelCommand;

        public bool IsCancelling
        {
            get
            {
                return _isCancelling;
            }
            private set
            {
                if (_isCancelling == value)
                    return;

                _isCancelling = value;
                OnPropertyChanged();
            }
        }

        public void Cancel()
        {
            _cancelationToken.Cancel();
            IsCancelling = true;
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
