﻿using System;
using System.Drawing;

namespace NPP
{
    public class Row
    {
        public DateTime Date { get; set; }
        public string ObjectA { get; set; }
        public string TypeA { get; set; }
        public string ObjectB { get; set; }
        public string TypeB { get; set; }
        public string Direction { get; set; }
        public Color Color { get; set; }
        public int Intensity { get; set; }
        public double LatitudeA { get; set; }
        public double LongitudeA { get; set; }
        public double LatitudeB { get; set; }
        public double LongitudeB { get; set; }
    }
}
