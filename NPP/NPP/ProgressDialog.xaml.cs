﻿using System.Threading;
using System.Windows;

namespace NPP
{
    /// <summary>
    /// Interaction logic for ProgressDialog.xaml
    /// </summary>
    public partial class ProgressDialog : Window
    {
        public ProgressDialog(CancellationTokenSource cancelationToken, Window owner)
        {
            InitializeComponent();
            Owner = owner;
            DataContext = new ProgressDialogModel(cancelationToken);
        }
    }
}
