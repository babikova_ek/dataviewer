﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Drawing;
using System.Globalization;

namespace NPP
{
    public class Parser : IDisposable
    {
        private TextFieldParser _textParser;
        private bool _first = true;

        public Parser(string fileName)
        {
            _textParser = new TextFieldParser(fileName)
            {
                TextFieldType = FieldType.Delimited
            };

            _textParser.SetDelimiters(";");
        }

        public bool EndOfFile => _textParser.EndOfData;

        public Row GetRow()
        {
            if (!_textParser.EndOfData)
            {
                var fields = _textParser.ReadFields();

                if (_first)
                {
                    _first = false;
                    return null;
                }

                var row = new Row
                {
                    Date = DateTime.ParseExact(fields[0], "dd.MM.yyyy", CultureInfo.InvariantCulture),
                    ObjectA = fields[1],
                    TypeA = fields[2],
                    ObjectB = fields[3],
                    TypeB = fields[4],
                    Direction = fields[5],
                    Color = ColorTranslator.FromHtml(fields[6]),
                    Intensity = Convert.ToInt16(fields[7]),
                    LatitudeA = Convert.ToDouble(fields[8], CultureInfo.InvariantCulture),
                    LongitudeA = Convert.ToDouble(fields[9], CultureInfo.InvariantCulture),
                    LatitudeB = Convert.ToDouble(fields[10], CultureInfo.InvariantCulture),
                    LongitudeB = Convert.ToDouble(fields[11], CultureInfo.InvariantCulture)
                };

                return row;
            }

            return null;
        }

        public void Dispose()
        {
            _textParser?.Dispose();
        }
    }
}
